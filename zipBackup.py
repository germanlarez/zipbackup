#!/usr/bin/env python3
  # zipBackup.py - Copies an entire folder and its contents into
  # a ZIP file whose filename increments.

import zipfile, os, logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s-%(levelname)s-%(message)s')

def backupToZip(folder):
  # Back up the entire contents of "folder" into a ZIP file.
  folder = os.path.abspath(folder) # returns absolute path of folder
  
  # Figure out the filename this code should use based on what files already exist.
  number = 1
  while True:
    zipFileName = os.path.basename(folder) + '_' + str(number) + '.zip'
    if not os.path.exists(zipFileName): # checks if the zip filename not exists and breaks for incrementing number as a suffix
      break
    number = number + 1
  
  # Creates a new ZIP file 
  print(f'Creating {zipFileName}...')
  backupZip = zipfile.ZipFile(zipFileName, 'w')
  print(f'Created {zipFileName} in {folder} directory...')

  # Walk the entire folder tree and compress the files in each folder
  for foldername, subfolders, filenames in os.walk(folder):
    print(f'Adding files in {foldername}...')
    backupZip.write(foldername)

    # Add all the files in this folder to the ZIP file.
    for filename in filenames:
      newBase = os.path.basename(folder) + '_'
      if filename.startswith(newBase) and filename.endswith('.zip'):
        continue # Don't back up ZIP files created before
      backupZip.write(os.path.join(foldername, filename))
  backupZip.close()
  print(f'Done compressing of {folder}')

# Call to execute the function
backupToZip('folderToZip')